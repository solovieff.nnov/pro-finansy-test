import os
import unittest

from dotenv import load_dotenv
from tinkoff.invest import Client, PortfolioResponse, MoneyValue

from profinansy import load_accounts, load_operations, form_operations_portfolio

load_dotenv()


class MyTestCase(unittest.TestCase):

    # getting portfolio data ourselves and then comparing to default portfolio api one
    def test_portfolio_positions_with_operations(self):
        with Client(os.environ["TINKOFF_API_TOKEN"]) as client:
            accounts = load_accounts(client)

            if not accounts:
                raise RuntimeError("Looks like you have no active accounts to process :(")

            operations = load_operations(client, accounts[0].id)

            assets_results = form_operations_portfolio(operations)
            print("Here go all our assets (one per figi):\n")
            for asset_pos in assets_results:
                asset_pos.print_main()

            asset_map = {item.figi: item for item in assets_results}

            r: PortfolioResponse = client.operations.get_portfolio(account_id=accounts[0].id)
            p_positions = r.positions

            print("\nTinkoff's average_position_price_fifo calc method seems "
                  "to mean something different from what we do, so we do not wait for the exact equality\n")
            for p in p_positions:
                # we skip currencies for now not to have problems with manually added and sold USD on my account
                if p.instrument_type == 'currency':
                    continue

                asset_position = asset_map[p.figi]
                print(f"checking {p.instrument_type} {p.figi}, {asset_position.name}...")

                print(f"our mean_buy_price vs their average_position_price_fifo "
                      f"{asset_position.mean_buy_price} vs {_get_float(p.average_position_price_fifo.units, p.average_position_price_fifo.nano)}")

                print(f"our quantity vs their quantity "
                      f"{asset_position.quantity} vs {_get_float(p.quantity.units, p.quantity.nano)} \n\n")
                # assert asset_position.quantity == _get_float(p.quantity.units, p.quantity.nano)

                # assert asset_position.mean_buy_price == _get_float_from_money_value(p.average_position_price_fifo)


# yes, yes, i know
def _get_float(int_part, nano):
    return int_part + nano / (10 ** len(str(nano)))


if __name__ == '__main__':
    unittest.main()
