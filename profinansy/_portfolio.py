import logging
import pprint
from dataclasses import dataclass
from datetime import datetime
from itertools import groupby
from typing import List

from tinkoff.invest import OperationItem, OperationType, MoneyValue

from profinansy import get_float_from_money_value


# representing lot
class BasePosition:
    quantity: float
    name: str
    figi: str
    price: MoneyValue
    date: datetime

    def __init__(self, item: OperationItem):
        self.quantity = item.quantity_done
        self.price = item.price
        self.figi = item.figi
        self.name = item.name
        self.date = item.date
        self.trades_info = item.trades_info


# representing full asset
@dataclass
class AssetPosition:
    figi: str
    name: str
    currency: str
    quantity: float
    lots: List[BasePosition]
    sum_open_cost: float

    @property
    def mean_buy_price(self):
        return self.sum_open_cost / self.quantity

    def print_main(self):
        pprint.pprint({'figi': self.figi, 'name': self.name, 'quantity': self.quantity,
                       'sum_open_cost': f"{self.sum_open_cost:,} {self.currency}",
                       'mean_buy_price': f"{self.mean_buy_price} {self.currency}", 'lot_count': len(self.lots)})


def form_operations_portfolio(operations_items: List[OperationItem]):
    """
    Collapsing sells (if any) with existing buys.
    If we have sell with quantity more than buys by that time, we treat the rest as a short sell and try to collapse it with future buys

    :param operations_items: all operations unfolded in terms of OperationItemTrades (one operation per trade)
    :return: List[AssetPosition] current portfolio positions (one per asset)
    """
    asset_data = []
    # grouping assets by figi
    sorted_data = sorted(operations_items, key=lambda x: x.figi)

    for key, group in groupby(sorted_data, lambda x: x.figi):
        if key != 'BBG008NVB1C0':
            # change pass to continue to check one asset only
            pass
        key_items = list(group)

        # sorting the items so first date goes first
        date_sorted_items = sorted(key_items, key=lambda x: x.date)
        print(f"processing {key}, {len(key_items)} operations total")

        full_positions = _process_asset_operations(date_sorted_items)
        base_positions = list(map(lambda item: BasePosition(item=item), full_positions))

        # removing zero quantities (it's collapsed buys/sells)
        base_positions = list(filter(lambda bp: bp.quantity != 0, base_positions))
        asset_position = _form_asset_position(base_positions)
        if asset_position:
            asset_data.append(asset_position)

    return asset_data


def _process_asset_operations(trade_requests):
    """
    Processing all operations for one asset

    :param trade_requests:
    :return: current asset positions with collapsed quantities set to zeroes
    """
    positions = []
    for request in trade_requests:
        if request.type == OperationType.OPERATION_TYPE_SELL:
            _sell(positions, request)
        else:
            _buy(positions, request)
        # shorting can go here too
        positions.insert(0, request)

    return positions

    # we care about quantities only here, due to we wont use other values anywhere else for our task


def _sell(cur_positions, sell_request):
    for pos in cur_positions:
        if pos.quantity_done <= 0:
            continue
        if sell_request.quantity_done <= pos.quantity_done:
            pos.quantity_done = pos.quantity_done - sell_request.quantity_done
            sell_request.quantity_done = 0
            break
        else:
            sell_request.quantity_done = sell_request.quantity_done - pos.quantity_done
            pos.quantity_done = 0
    sell_request.quantity_done *= (-1)

    # we care about quantities only here, due to we wont use other values anywhere else for our task
    # mostly to be accurate with short sells


def _buy(cur_positions, buy_request):
    for pos in cur_positions:
        if pos.quantity_done >= 0:
            continue
        # trying to collapse short sells, too
        if buy_request.quantity_done <= abs(pos.quantity_done):
            pos.quantity_done = pos.quantity_done + buy_request.quantity_done  # still negative
            buy_request.quantity_done = 0
            break
        else:
            buy_request.quantity_done = buy_request.quantity_done + pos.quantity_done
            pos.quantity_done = 0


def _form_asset_position(asset_base_positions: List[BasePosition]) -> AssetPosition:
    if not asset_base_positions:
        return None

    quantity = 0
    sum_market_value = 0
    currency = asset_base_positions[0].price.currency
    name = asset_base_positions[0].name
    figi = asset_base_positions[0].figi

    for bp in asset_base_positions:
        sum_market_value += bp.quantity * get_float_from_money_value(bp.price)
        quantity += bp.quantity
    return AssetPosition(quantity=quantity, figi=figi, name=name, sum_open_cost=sum_market_value,
                         lots=asset_base_positions, currency=currency)
