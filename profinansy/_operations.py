import dataclasses
import os
import pprint
from typing import List

from dotenv import load_dotenv

from tinkoff.invest import Client, RequestError, AccessLevel, GetOperationsByCursorRequest, OperationItem, \
    OperationType, OperationState, InstrumentType, MoneyValue


def load_operations(tinkoff_client: Client, account_id, unfold_trades=True, skip_currencies=True) -> List[
    OperationItem]:
    """
    Loading all operations for given account_id. Assuming for now we need only executed BUY and SELL operations, no margins etc.

    :param tinkoff_client: tinkoff.invest.Client
    :param account_id: str
    :param unfold_trades: bool if to unfold trade info or just use operation itself
    :param skip_currencies: bool if to skip currencies exchange. adding currency to the account is not listed, so all sells go alone.
            In general we better skip this..
    :return: List[OperationItem]
    """

    def get_ops_request(cursor=""):
        return GetOperationsByCursorRequest(
            account_id=account_id,
            cursor=cursor
        )

    ops = tinkoff_client.operations.get_operations_by_cursor(get_ops_request())
    items = ops.items

    # running until we get them all!
    while ops.has_next:
        request = get_ops_request(cursor=ops.next_cursor)
        ops = tinkoff_client.operations.get_operations_by_cursor(request)
        items += ops.items

    # filtering
    filtered_items = list(filter(
        lambda item: (item.type in [OperationType.OPERATION_TYPE_BUY,
                                    OperationType.OPERATION_TYPE_SELL]
                      and item.state == OperationState.OPERATION_STATE_EXECUTED and (
                              skip_currencies and item.instrument_kind != InstrumentType.INSTRUMENT_TYPE_CURRENCY)),
        items))
    # this can be done more accurate actually
    if unfold_trades:
        unfolded_items = []
        for item in filtered_items:
            unfolded_items += _unfold_trade_info(item)
        return unfolded_items
    else:
        return filtered_items


def load_accounts(tinkoff_client: Client):
    """

    :param tinkoff_client: tinkoff.invest.Client
    :return: list of accounts that we have access to
    """
    acc_response = tinkoff_client.users.get_accounts()
    return list(
        filter(lambda acc: acc.access_level != AccessLevel.ACCOUNT_ACCESS_LEVEL_NO_ACCESS, acc_response.accounts))


# yes, yes, i know
def _get_float(int_part, nano):
    return int_part + nano / (10 ** len(str(nano)))


# this can be done another way, but for speed let's use float
def get_float_from_money_value(mv: MoneyValue):
    return mv.units + mv.nano / (10 ** len(str(mv.nano)))


def _unfold_trade_info(operation_item: OperationItem):
    """
    creating several trade items using trade_info array
    :param operation_item:
    :return: list of operation_items created for each trade info
    """
    unfolded = []
    for trade_info in operation_item.trades_info.trades:
        operation_item_copy = dataclasses.replace(operation_item)
        operation_item_copy.date = trade_info.date
        operation_item_copy.price = trade_info.price
        operation_item_copy.quantity_done = trade_info.quantity
        unfolded.append(operation_item_copy)
    return unfolded
